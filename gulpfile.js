let { series, watch, src, dest } = require('gulp');
let uglify = require('gulp-uglify');
let sass = require('gulp-sass');
let browsersync = require('browser-sync').create();
const postcss = require('gulp-postcss');
const cssnano = require('cssnano');





function clean(cb) {
    cb();
};

// Sass Task
function scssTask(){
    return src('./css/style.scss', { sourcemaps: true })
        .pipe(sass())
        .pipe(postcss([cssnano()]))
        .pipe(dest('dist', { sourcemaps: '.' }));
  };

// JavaScript Task
function jsTask(){
    return src('./js/*.js', { sourcemaps: true })
      .pipe(terser())
      .pipe(dest('dist', { sourcemaps: '.' }));
  }


function uglifyTask (cb) {
    return src('src/*.js')
      // The gulp-uglify plugin won't update the filename
      .pipe(uglify())
      // So use gulp-rename to change the extension
      .pipe(rename({ extname: '.min.js' }))
      .pipe(dest('dist/'));
      cb();
  };
 
  function watchTask(){
    watch('*.html', browsersyncReload);
    watch(['./css/*.scss', './js/*.js'], series(scssTask, jsTask, browsersyncReload));
  }

  function browsersyncServe(cb){
    browsersync.init({
      server: {
        baseDir: '.'
      }    
    });
    cb();
  };

  function browsersyncReload(cb){
    browsersync.reload();
    cb();
  }


  exports.default = series(browsersyncServe, clean, watchTask, scssTask, jsTask);